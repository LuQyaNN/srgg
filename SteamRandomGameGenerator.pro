#-------------------------------------------------
#
# Project created by QtCreator 2015-07-11T00:07:56
#
#-------------------------------------------------

QT       += core gui network



greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SteamRandomGameGenerator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    aboutwindow.cpp

HEADERS  += mainwindow.h \
    aboutwindow.h

FORMS    += mainwindow.ui \
    aboutwindow.ui

CONFIG += static

RESOURCES += \
    resurce.qrc
