#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QNetworkReply>
#include <QNetworkDiskCache>
#include <QFile>
#include <QPointer>
#include <QTimer>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    //QNetworkDiskCache* cache;
    QNetworkAccessManager* NetworkAccessManagerSteamApi
    ,*NetworkAccessManagerSteamGamePage ;
    QPointer<QNetworkReply> SteamApiReply;
    QVector<QPointer<QNetworkReply> > SteamRedirectPageReply;
    QVector<QPointer<QNetworkReply> > SteamGamePageReply;
    bool stoped;
    QTimer* PageReplyTimer;
    QWidget* focusedw;
public slots:
    void ReplySteamApi(QNetworkReply* NetworkReply);
    void ReplyFinished();
    void ReplySteamGamePage(QNetworkReply* NetworkReply);
    void ProcessSteamApi(QString& strReply);
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);

private slots:

    void on_pushGenerateButton_clicked();

    void on_listWidget_doubleClicked(const QModelIndex &index);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();
    void StartConnects();
    void on_listWidget_2_doubleClicked(const QModelIndex &index);

    void on_lineGenreEdit_editingFinished();

    void on_lineGenreDecideEdit_editingFinished();

    void on_pushButton_3_clicked();

private:
    QVector<QUrl> urls;
    QFile BlackListFile;

    //��, ������� ������ �� ����� �������������� � ������� �������
    QVector<int> CachedIds;
    QVector<int> BlackListId;


    int finishedcount;
    QVector<int> ids;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
