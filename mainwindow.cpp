#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <sstream>
#include "QTime"
#include "iostream"
#include "aboutwindow.h"
#include <QTextStream>
#include <QDebug>


#include <QDesktopServices>
#include <document.h>
#include <filestream.h>
#include <prettywriter.h>
#include <rapidjson.h>
#include <reader.h>
#include <stringbuffer.h>
#include <writer.h>
#include <internal\pow10.h>
#include <internal\stack.h>
#include <internal\strfunc.h>


using namespace rapidjson;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    NetworkAccessManagerSteamApi = new QNetworkAccessManager(this);
    connect(NetworkAccessManagerSteamApi, SIGNAL(finished(QNetworkReply*)),this, SLOT(ReplySteamApi(QNetworkReply*)));
    NetworkAccessManagerSteamGamePage = new QNetworkAccessManager(this);
    connect(NetworkAccessManagerSteamGamePage, SIGNAL(finished(QNetworkReply*)),this, SLOT(ReplySteamGamePage(QNetworkReply*)));
    //cache = new QNetworkDiskCache(this);

    //cache->setCacheDirectory("Cache");

    //NetworkAccessManagerSteamGamePage->setCache(cache);
    PageReplyTimer = new QTimer(this);
    connect(PageReplyTimer,SIGNAL(timeout()),this,SLOT(StartConnects()));
    SteamApiReply = 0;

    stoped = false;

    QFile file("genreslist");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        QStringList genrestr;
        genrestr = in.readAll().split(";",QString::SkipEmptyParts);
        ui->listWidget_2->addItems(genrestr);
        file.close();
    }
}

void MainWindow::ReplySteamApi(QNetworkReply* NetworkReply)
{

    if(NetworkReply->error()!=0)
    {
        qDebug()<<NetworkReply->error();
        return;
    }

    QString strReply;
    strReply= (QString)NetworkReply->readAll();

    QFile file("gamelist");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        return;
    QTextStream in(&file);
    in<<strReply;
    file.close();

    ProcessSteamApi(strReply);
    NetworkReply->deleteLater();



}

void MainWindow::ReplyFinished()
{
    finishedcount++;
    if(!stoped)
        ui->labelCount_8->setText(QString::number(finishedcount)+QString("/")+QString::number(ids.size()));

    if(ui->listWidget->count()>=ui->spinBox->text().toInt())
    {
        ui->labelReady_9->setText(QString("Ready"));
        NetworkAccessManagerSteamGamePage->clearAccessCache();
        stoped = true;
        PageReplyTimer->stop();
    }
}


void MainWindow::ReplySteamGamePage(QNetworkReply* NetworkReply)
{

    /*
    QTime time = QTime::currentTime();
    qsrand(time.msec());

    if(ui->listWidget->count()<ui->spinBox->text().toInt())
    {
        for(int i =0;i<20;++i){
            if(stoped)
                return;
            int id = ids[qrand()%ids.size()];
            if(BlackListId.contains(id))
                continue;
            if(!CachedIds.contains(id)){
                CachedIds.append(id);
                QNetworkReply* reply = NetworkAccessManagerSteamGamePage->get(
                            QNetworkRequest(QString("http://store.steampowered.com/api/appdetails?appids=")
                                            +QString::number(id)+QString("&cc=ru")));
                connect(reply,SIGNAL(finished()),this,SLOT(ReplyFinished()));
                SteamGamePageReply.append(reply);


            }
        }
    }
    if(NetworkReply->error()!=0)
        return;

    if(ui->listWidget->count()>=ui->spinBox->text().toInt())
        return;

    QString strReply = (QString)NetworkReply->readAll();

    Document d;
    d.Parse<rapidjson::kParseDefaultFlags>(strReply.toUtf8().toStdString().c_str());
    if(!d.MemberBegin()->value["success"].GetBool())
        return;

    if(d.MemberBegin()->value["data"]["type"].GetString() != QString("game"))
        return;

    QString name = d.MemberBegin()->value["data"]["name"].GetString();
    qDebug()<<name;

    QString date = d.MemberBegin()->value["data"]["release_date"]["date"].GetString();

    QStringList genres;
    if(d.MemberBegin()->value["data"]["genres"].IsArray())
    {
        for(Value::ConstValueIterator i = d.MemberBegin()->value["data"]["genres"].Begin();
            i!=d.MemberBegin()->value["data"]["genres"].End();i++)
        {
            genres.append((*i)["description"].GetString());
        }
    }



    bool genreacepted = true;
    QStringList genrelist = ui->lineGenreEdit->text().split(";",QString::SkipEmptyParts);

    for(int i = 0;i<genrelist.size();++i)
    {
        genreacepted &= genres.contains(genrelist[i]);
    }
    if(!genreacepted)
        return;


    for(int i = ui->spinDateMinBox->text().toInt();i<=ui->spinDateMaxBox_2->text().toInt();++i)
    {
        if(date.contains(QString::number(i)))
        {
            if(ui->listWidget->count()<ui->spinBox->text().toInt())
            {
                urls.append(QString("http://store.steampowered.com/app/")+QString::number(d.MemberBegin()->value["data"]["steam_appid"].GetInt()));
                ui->listWidget->addItem(name);
                ui->labelAcceptednum_5->setText(QString::number(ui->listWidget->count()));

            }
        }
    }
*/

    qDebug()<<NetworkReply->url().toString();
    qDebug()<<"Redirection "<<NetworkReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString();
    if(NetworkReply->error()!=0)
        return;

    if(ui->listWidget->count()>=ui->spinBox->text().toInt())
        return;

    QString strReply = (QString)NetworkReply->readAll();

    //����� �������
    if(NetworkReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString().contains("video",Qt::CaseInsensitive))
    {
        BlackListId.append(NetworkReply->url().toString().remove(0,NetworkReply->url().toString().lastIndexOf(QString("/"))+1).toInt());
        return;
    }
    //�������� �� �������� ���� ����� �������� ��������
    if(NetworkReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString().contains("http://store.steampowered.com/app/",Qt::CaseInsensitive))
    {
        QNetworkRequest Request(NetworkReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString());
        QNetworkReply* reply = NetworkAccessManagerSteamGamePage->get(Request);
        connect(reply,SIGNAL(finished()),this,SLOT(ReplyFinished()));
        SteamRedirectPageReply.append(reply);
        return;
    }
    //�������� ��� �������
    if(NetworkReply->url().toString().contains("agecheck",Qt::CaseInsensitive))
    {
        QNetworkRequest Request(NetworkReply->url().toString());
        QNetworkReply* reply = NetworkAccessManagerSteamGamePage->post(Request,"snr=1_agecheck_agecheck__age-gate&ageDay=1&ageMonth=January&ageYear=1961");
        connect(reply,SIGNAL(finished()),this,SLOT(ReplyFinished()));
        SteamRedirectPageReply.append(reply);
        return;
    }
    //�������� �� �������� �� �������� ��������
    if(NetworkReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString().contains("agecheck",Qt::CaseInsensitive))
    {
        QNetworkRequest Request(NetworkReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString());
        QNetworkReply* reply = NetworkAccessManagerSteamGamePage->get(Request);
        connect(reply,SIGNAL(finished()),this,SLOT(ReplyFinished()));
        SteamRedirectPageReply.append(reply);
        return;
    }



    //�������� �� ���������
    if(strReply.contains("http://store.steampowered.com/search/?snr=1_5_9__205#category1=994",Qt::CaseInsensitive))
    {
        BlackListId.append(NetworkReply->url().toString().remove(0,NetworkReply->url().toString().lastIndexOf(QString("/"))+1).toInt());
        qDebug()<<"APP "<<NetworkReply->url().toString();
        return;
    }

    //�������� �� DLC
    if(strReply.contains("http://store.akamai.steamstatic.com/public/images/ico/ico_dlc.gif",Qt::CaseInsensitive))
    {
        BlackListId.append(NetworkReply->url().toString().remove(0,NetworkReply->url().toString().lastIndexOf(QString("/"))+1).toInt());
        qDebug()<<"DLC "<<NetworkReply->url().toString();
        return;
    }

    int datepos = -1;
    datepos  = strReply.indexOf(QString::fromLatin1("details_block"),0,Qt::CaseInsensitive);

    //���������� � �������
    if(datepos>-1)
    {
        QString datestr = strReply;
        qDebug()<<datepos;
        datestr.remove(0,datepos);
        datestr.remove(datestr.indexOf(QString("</div>")),datestr.count());
        //�������� ������
        bool genreacepted = true;
        QStringList genrelist = ui->lineGenreEdit->text().split(";",QString::SkipEmptyParts);
        for(int i = 0;i<genrelist.size();++i)
        {
            genreacepted &= datestr.contains(QString(">")+genrelist[i]+QString("</a>"));
        }
        QStringList genredecidelist = ui->lineGenreDecideEdit->text().split(";",QString::SkipEmptyParts);
        for(int i = 0;i<genredecidelist.size();++i)
        {
            genreacepted &= !datestr.contains(QString(">")+genredecidelist[i]+QString("</a>"));
        }
        if(!genreacepted)
            return;


        for(int i = ui->spinDateMinBox->text().toInt();i<=ui->spinDateMaxBox_2->text().toInt();++i)
        {
            if(datestr.contains(QString::number(i)))
            {
                if(ui->listWidget->count()<ui->spinBox->text().toInt())
                {
                    QString namestr = strReply;
                    int namestartpos = namestr.indexOf(QString::fromLocal8Bit("apphub_AppName"),0,Qt::CaseInsensitive);
                    namestr.remove(0,namestartpos+16);
                    int nameendpos = namestr.indexOf(QString::fromLocal8Bit("</div>"),0,Qt::CaseInsensitive);
                    namestr.remove(nameendpos,namestr.count());


                    urls.append(NetworkReply->url());
                    ui->listWidget->addItem(namestr);
                    ui->labelAcceptednum_5->setText(QString::number(ui->listWidget->count()));

                }
            }
        }
    }
    else
    {
        BlackListId.append(NetworkReply->url().toString().remove(0,NetworkReply->url().toString().lastIndexOf(QString("/"))+1).toInt());
    }



}

void MainWindow::ProcessSteamApi(QString &strReply)
{

    ui->pushGenerateButton->setEnabled(true);
    Document d;
    d.Parse<rapidjson::kParseDefaultFlags>(strReply.toUtf8().toStdString().c_str());

    Value& Value1 = d["applist"];
    Value& Value2 = Value1["apps"];

    for(Value::ConstValueIterator a = Value2.Begin();a!=Value2.End();a++)
    {
        for(Value::ConstMemberIterator b = a->MemberBegin();b!=a->MemberEnd();b++)
        {
            if(b->value.IsInt64())
                ids.append(b->value.GetInt64());
        }
    }

    PageReplyTimer->start(2000);

}

void MainWindow::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    ui->progressBar->setMaximum(bytesTotal);
    ui->progressBar->setValue(bytesReceived);
}

MainWindow::~MainWindow()
{

    on_pushButton_clicked();
    BlackListFile.close();
    delete ui;
    delete NetworkAccessManagerSteamApi;
    delete NetworkAccessManagerSteamGamePage ;
}

void MainWindow::on_pushGenerateButton_clicked()
{
    //nulls
    on_pushButton_clicked();


    urls.clear();

    ui->pushGenerateButton->setEnabled(false);
    ui->progressBar->setValue(0);
    ui->listWidget->clear();

    finishedcount = 0;
    stoped = false;
    ui->labelAcceptednum_5->setText(QString::number(0));
    ui->labelCount_8->setText(QString::number(0));
    ui->labelReady_9->setText(QString("In Progress"));

    //open blacklist
    BlackListFile.setFileName("blacklist");
    if (!BlackListFile.open(QIODevice::ReadWrite | QIODevice::Text))
        return;
    QTextStream  BlackListStream(&BlackListFile);
    BlackListStream.reset();
    //read blacklist
    QString bl = (QString)BlackListStream.readAll();
    QStringList sl = bl.split("\n",QString::SkipEmptyParts);

    for(int i =0;i<sl.size();++i)
        BlackListId.append(sl[i].toInt());

    //start request
    if(!ui->checkBox->isChecked())
    {
        QNetworkRequest request(QUrl(QString("http://api.steampowered.com/ISteamApps/GetAppList/v2/?key=51D62385ADEF34D837D189FC30A3224C&format=json")));
        SteamApiReply = NetworkAccessManagerSteamApi->get(request);
        connect(SteamApiReply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(downloadProgress(qint64,qint64)));


    }
    else
    {

        QFile file("gamelist");

        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QNetworkRequest request(QUrl(QString("http://api.steampowered.com/ISteamApps/GetAppList/v2/?key=51D62385ADEF34D837D189FC30A3224C&format=json")));
            SteamApiReply = NetworkAccessManagerSteamApi->get(request);
            connect(SteamApiReply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(downloadProgress(qint64,qint64)));
            return;
        }
        QTextStream in(&file);
        QString strReply;

        strReply = in.readAll();

        file.close();
        ProcessSteamApi(strReply);



    }


}

void MainWindow::on_listWidget_doubleClicked(const QModelIndex &index)
{
    QDesktopServices::openUrl(urls[index.row()]);

}

void MainWindow::on_pushButton_clicked()
{
    PageReplyTimer->stop();
    ids.clear();
    CachedIds.clear();
    ui->labelReady_9->setText(QString("Stoping..."));
    stoped = true;

    NetworkAccessManagerSteamApi->clearAccessCache();
    if(SteamApiReply)
    {
        delete SteamApiReply;

    }

    for(int i =0;i<SteamGamePageReply.size();++i)
    {
        delete SteamGamePageReply[i];
    }
    NetworkAccessManagerSteamGamePage->clearAccessCache();


    BlackListFile.setFileName("blacklist");
    if (!BlackListFile.open(QIODevice::ReadWrite | QIODevice::Text))
        return;
    QTextStream  BlackListStream(&BlackListFile);

    for(int i =0;i<BlackListId.size();++i)
    {
        BlackListStream<<BlackListId[i]<<endl;
    }
    BlackListFile.close();



    BlackListId.clear();
    ui->progressBar->setValue(0);
    ui->pushGenerateButton->setEnabled(true);
    ui->labelReady_9->setText(QString("Ready"));
}

void MainWindow::on_pushButton_2_clicked()
{
    for(int i = 0; i<urls.size();++i)
        QDesktopServices::openUrl(urls[i]);
}

void MainWindow::StartConnects()
{
    SteamGamePageReply.resize(50);
    QTime time = QTime::currentTime();
    qsrand(time.msec());
    for(int i =0;i<50;++i)
    {

        if(!SteamGamePageReply[i] || SteamGamePageReply[i]->isFinished())
        {
            int id = 0;
            delete SteamGamePageReply[i];
            do{
                id = ids[qrand()%ids.size()];
            }while(BlackListId.contains(id) && !CachedIds.contains(id));
            CachedIds.append(id);
            QNetworkRequest Request(QString("http://store.steampowered.com/app/")+QString::number(id));
            SteamGamePageReply[i] = NetworkAccessManagerSteamGamePage->get(Request);
            connect(SteamGamePageReply[i],SIGNAL(finished()),this,SLOT(ReplyFinished()));
        }
    }

    for(int i =0;i<SteamRedirectPageReply.size();++i)
    {
        if(!SteamRedirectPageReply[i] || SteamRedirectPageReply[i]->isFinished())
        {
            delete SteamRedirectPageReply[i];
            SteamRedirectPageReply.removeAt(i);
        }
    }
}

void MainWindow::on_listWidget_2_doubleClicked(const QModelIndex &index)
{
    if(focusedw == ui->lineGenreEdit)
        ui->lineGenreEdit->setText(ui->lineGenreEdit->text()+index.data().toString()+QString(";"));
    if(focusedw == ui->lineGenreDecideEdit)
        ui->lineGenreDecideEdit->setText(ui->lineGenreDecideEdit->text()+index.data().toString()+QString(";"));

}


void MainWindow::on_lineGenreEdit_editingFinished()
{
    focusedw = ui->lineGenreEdit;
}

void MainWindow::on_lineGenreDecideEdit_editingFinished()
{
    focusedw = ui->lineGenreDecideEdit;
}

void MainWindow::on_pushButton_3_clicked()
{
    AboutWindow aboutwindow;
    aboutwindow.exec();
}
